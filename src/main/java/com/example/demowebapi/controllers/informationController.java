package com.example.demowebapi.controllers;

import com.example.demowebapi.models.article;
import com.example.demowebapi.repositorys.articleRepository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class informationController {
    @Autowired
    articleRepository rep;

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @RequestMapping(value = "/list-author", method = RequestMethod.GET)
    public List<article> index() {
        List<article> list = new ArrayList<>();
        list = rep.findAll();
		return list;
    }
}
